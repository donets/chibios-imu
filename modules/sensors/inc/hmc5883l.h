/*
 * hmc5883l.h
 *
 *  Created on: 11 мая 2015 г.
 *      Author: Administrator
 */

#ifndef _HMC5883L_H_
#define _HMC5883L_H_

/* buffers depth */
#define RX_BUFFER_SIZE 6
#define TX_BUFFER_SIZE 2

#define HMC5883L_DEFAULT_ADDRESS 0x1E   // Адрес 0х1E без восьмого бита чтения/записи адрес на запись 0x3C
                                        // на чтение 0x3D (0x1E << 1)

typedef enum {      // Структура адресов микросхемы HMC5883L
    HMC5883L_CONF_REGA = 0,
    HMC5883L_CONF_REGB,
    HMC5883L_MODE_REG,
    HMC5883L_DATA_OUT_X_MSB_REG,
    HMC5883L_DATA_OUT_X_LSB_REG,
    HMC5883L_DATA_OUT_Z_MSB_REG,
    HMC5883L_DATA_OUT_Z_LSB_REG,
    HMC5883L_DATA_OUT_Y_MSB_REG,
    HMC5883L_DATA_OUT_Y_LSB_REG,
    HMC5883L_STATUS_REG,
    HMC5883L_IDENT_REGA,
    HMC5883L_IDENT_REGB,
    HMC5883L_IDENT_REGC
} HMC5883L_Registers;



#endif /* _HMC5883L_H_ */
