/*
 * i2c.c
 *
 *  Created on: 12 мая 2015 г.
 *      Author: Administrator
 */

#include "ch.h"
#include "hal.h"
#include "i2c_loc.h"

static i2cflags_t errors = 0;
msg_t status = RDY_OK;      // Флаг статуса передачи по I2C

// Инициализируем I2C1
void I2C1_Init() {
    static const I2CConfig i2c1_config = {  //Заполняем структуру параметров I2C
            OPMODE_I2C,     //Параметр управляет режимом, биты 0-1 и 3 регистра I2C_CR1
            100000,         //Установка скорости передачи по I2C
            STD_DUTY_CYCLE  //Параметр управления рабочим циклом, регистр I2C_CCR (Clock Control Register)
    };
    //Явно определяем альтернативную функцию для выводов, которые используют I2C. Настройка PAL driver
    palSetPadMode(GPIOB, 6, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN); //SCL
    palSetPadMode(GPIOB, 9, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN); //SDA
    chThdSleepMilliseconds(100);    //Немного ждем

    i2cStart(&I2C_DRIVER, &i2c1_config);    //Запускаем I2C
}

void HMC5883L_I2C_BytesWrite(uint8_t slaveAddr, uint8_t* txBuffer) {
    i2cAcquireBus(&I2C_DRIVER); // Получение эксклюзивного доступа к шине I2C
                                // In order to use this function the option I2C_USE_MUTUAL_EXCLUSION must be enabled.

    /* Функция #define I2C_SEND i2cMasterTransmitTimeout возвращает в status следующие значения
     * RDY_OK       если функция завершилась успешно
     * RDY_RESET    если функция выдала одну или несколько ошибок, ошибки можно получить вызовом i2cGetErrors()
     * RDY_TIMEOUT  истек таймаут до окончания операции передачи, скорее всего проблема в физическом подключении
     */
    status = I2C_SEND   ( &I2C_DRIVER,  // Указатель на объект I2CDriver
                          slaveAddr,    // Slave адрес устройства (7 бит) без бита чтения/записи R/W
                          txBuffer,     // Указатель на буффер передачи
                          2,            // Количество байт для передачи
                          rxbuf,        // Указатель на приемный буффер
                          0,            // Количество байт для приема, 0 если делаем только передачу
                          TIMEOUT       // Количество тиков перед операцией таймаута, доступно TIME_INFINITE без таймаута
                        );

    i2cReleaseBus(&I2C_DRIVER); // Освобождает доступ к шине I2C
                                // In order to use this function the option I2C_USE_MUTUAL_EXCLUSION must be enabled.

    if (status != RDY_OK) { // если функция завершилась с ошибками, получаем ошибки
        errors = i2cGetErrors(&I2C_DRIVER);
    }
}
/* Проверка на ошибки
 *  Функция i2cMasterTransmitTimeout может возвращать следующие значения:
 *   RDY_OK = 0
 *   RDY_TIMEOUT = -1
 *   RDY_RESET = -2
 *  В случае получения RDY_RESET следует получить расширенный код ошибки:
 *   i2cflags_t errors = i2cGetErrors(&I2C_DRIVER);
 *  Расширенный код ошибки определён так:
 *   #define I2CD_NO_ERROR               0x00   // Нет ошибки
 *   #define I2CD_BUS_ERROR              0x01   // Ошибка шины
 *   #define I2CD_ARBITRATION_LOST       0x02   // Потеря арбитража
 *   #define I2CD_ACK_FAILURE            0x04   // Сбой подтверждения
 *   #define I2CD_OVERRUN                0x08   // Пере- или недополнение
 *   #define I2CD_PEC_ERROR              0x10   // PEC ошибка при приёме
 *   #define I2CD_TIMEOUT                0x20   // Таймаут в железе
 *   #define I2CD_SMB_ALERT              0x40   // Тревога SMBus
 */

uint8_t HMC5883L_I2C_BytesRead(uint8_t slaveAddr, uint8_t* txBuffer) {
    uint8_t rxBuffer = 0;
    i2cAcquireBus(&I2C_DRIVER); // Получение эксклюзивного доступа к шине I2C
                                // In order to use this function the option I2C_USE_MUTUAL_EXCLUSION must be enabled.

    /* Функция #define I2C_SEND i2cMasterTransmitTimeout возвращает в status следующие значения
     * RDY_OK       если функция завершилась успешно
     * RDY_RESET    если функция выдала одну или несколько ошибок, ошибки можно получить вызовом i2cGetErrors()
     * RDY_TIMEOUT  истек таймаут до окончания операции передачи, скорее всего проблема в физическом подключении
     */
    status = I2C_SEND   ( &I2C_DRIVER,  // Указатель на объект I2CDriver
                          slaveAddr,    // Slave адресс устройства (7 бит) без бита чтения/записи R/W
                          txBuffer,     // Указатель на буффер передачи
                          1,            // Количество байт для передачи
                          rxBuffer,     // Указатель на приемный буффер
                          1,            // Количество байт для приема, 0 если делаем только передачу
                          TIMEOUT       // Количество тиков перед операцией таймаута, доступно TIME_INFINITE без таймаута
                        );

    i2cReleaseBus(&I2C_DRIVER); // Освобождает доступ к шине I2C
                                // In order to use this function the option I2C_USE_MUTUAL_EXCLUSION must be enabled.

    if (status != RDY_OK) { // если функция завершилась с ошибками, получаем ошибки
        errors = i2cGetErrors(&I2C_DRIVER);
        chprintf((BaseSequentialStream *) &SD2, " %x", errors);
    }

    return rxBuffer;
}

void HMC5883L_I2C_DataRead(uint8_t slaveAddr, uint8_t bytesNum) {
    //i2cAcquireBus(&I2C_DRIVER);     // Получение эксклюзивного доступа к шине I2C
                                    // In order to use this function the option I2C_USE_MUTUAL_EXCLUSION must be enabled.
    /**
     * i2cMasterReceiveTimeout -- функция чтения в буфер приема 6-ти байт
     */
    status = i2cMasterReceiveTimeout    ( &I2C_DRIVER,  // Указатель на объект I2CDriver
                                          slaveAddr,    // Slave адресс устройства (7 бит) без бита чтения/записи R/W
                                          rxbuf,        // Указатель на буффер передачи
                                          bytesNum,     // Количество байт для приема
                                          TIMEOUT       // Количество тиков перед операцией таймаута, доступно TIME_INFINITE без таймаута
                                        );
    //i2cReleaseBus(&I2C_DRIVER);     // Освобождает доступ к шине I2C

    if (status != RDY_OK) { // если функция завершилась с ошибками, получаем ошибки
        errors = i2cGetErrors(&I2C_DRIVER);
        chprintf((BaseSequentialStream *) &SD2, " %x", errors);
    }
}
